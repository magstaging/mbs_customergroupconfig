<?php
declare(strict_types=1);

namespace Mbs\CustomConfig\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReadCustomerGroups extends Command
{
    private \Mbs\CustomConfig\Model\Config\Data $dataConfig;

    /**
     * ReadCustomerGroups constructor.
     * @param \Mbs\CustomConfig\Model\Config\Data $dataConfig
     * @param string|null $name
     */
    public function __construct(
        \Mbs\CustomConfig\Model\Config\Data $dataConfig,
        string $name = null
    ) {
        parent::__construct($name);
        $this->dataConfig = $dataConfig;
    }

    protected function configure()
    {
        $this->setName('mbs:config:findcustomergroups');
        $this->setDescription('Find all the customer groups in the config');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customerGroups = $this->dataConfig->get();

        foreach ($customerGroups as $group) {
            $output->writeln('customer group: ' . $group['label']);
        }

        $output->writeln('Customer groups all read');
    }
}
