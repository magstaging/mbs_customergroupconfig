<?php
declare(strict_types=1);

namespace Mbs\CustomConfig\Model\Config;

interface DataInterface
{
    /**
     * Get configuration of all registered customer groups
     *
     * @return array
     */
    public function getAll();
}
