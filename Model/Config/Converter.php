<?php
declare(strict_types=1);

namespace Mbs\CustomConfig\Model\Config;

class Converter implements \Magento\Framework\Config\ConverterInterface
{
    /**
     * Convert dom node tree to array
     *
     * @param \DOMDocument $source
     * @return array
     */
    public function convert($source)
    {
        $customerGroupInfo = [];

        /** @var DOMNode $groupNode */
        foreach ($source->documentElement->childNodes as $groupNode) {
            if ($groupNode->nodeType != XML_ELEMENT_NODE) {
                continue;
            }

            $groupInfo = [];

            /** @var DOMNode $groupAttributeNode */
            foreach ($groupNode->childNodes as $groupAttributeNode) {
                if ($groupAttributeNode->nodeType != XML_ELEMENT_NODE) {
                    continue;
                }

                $groupInfo[(string) $groupAttributeNode->nodeName] = (string) $groupAttributeNode->nodeValue;
            }

            $customerGroupInfo[] = $groupInfo;
        }
        return $customerGroupInfo;
    }
}
