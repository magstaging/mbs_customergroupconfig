# README #

Add a custom config to define some customer groups that can be used for marketing features.
On this occasion, the module is just an example to illustrate how to create and 
use a custom configuration in Magento 2

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/Mbs_CustomerGroupConfig when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

php bin/magento mbs:config:findcustomergroups renders all the customers groups defined